#!/bin/bash

set -eu
cd /app/code

echo "=> Ensure directories"
mkdir -p /run/freescout/sessions \
    /app/data/bootstrap-cache \
    /app/data/storage/app/public/ \
    /app/data/storage/framework/cache \
    /app/data/storage/framework/views \
    /app/data/storage/framework/session \
    /app/data/public-css-builds \
    /app/data/public-js-builds \
    /app/data/public-modules \
    /app/data/Modules

echo -e "[client]\npassword=${CLOUDRON_MYSQL_PASSWORD}" > /run/freescout/mysql-extra
readonly MYSQL="mysql --defaults-file=/run/freescout/mysql-extra --user=${CLOUDRON_MYSQL_USERNAME} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -e "

echo "=> Ensure permissions"
chown -R www-data:www-data /app/data /run/freescout

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

if [[ ! -f /app/data/.installed ]]; then
    echo "=> First time use"

    mkdir -p /app/data/storage
    cp -R /app/pkg/storage.template/* /app/data/storage

    cp /app/code/.env.example /app/data/env
    chown -R www-data:www-data /app/data /run/freescout

    echo "=> Generate app key"
    sudo -E -u www-data php artisan key:generate --no-interaction --force

    touch /app/data/.installed
fi

if [[ ! -d /app/data/resources-lang ]]; then
    echo "=> Copy initial translation resources"
    mkdir -p /app/data/resources-lang
    cp -R /app/pkg/resources-lang.template/* /app/data/resources-lang
fi

echo "=> Set configs"
crudini --set /app/data/env "" APP_URL "${CLOUDRON_APP_ORIGIN}"
crudini --set /app/data/env "" APP_FORCE_HTTPS "true"
crudini --set /app/data/env "" DB_CONNECTION "mysql"
crudini --set /app/data/env "" DB_HOST "${CLOUDRON_MYSQL_HOST}"
crudini --set /app/data/env "" DB_PORT "${CLOUDRON_MYSQL_PORT}"
crudini --set /app/data/env "" DB_DATABASE "${CLOUDRON_MYSQL_DATABASE}"
crudini --set /app/data/env "" DB_USERNAME "${CLOUDRON_MYSQL_USERNAME}"
crudini --set /app/data/env "" DB_PASSWORD "${CLOUDRON_MYSQL_PASSWORD}"
crudini --set /app/data/env "" APP_DISABLE_UPDATING "true"

echo "=> Clear cache folders"
# https://github.com/freescout-help-desk/freescout/blob/a49fd0ad33889cb18cbe407af36259a3c3aa6287/tools/update.sh#L197
rm -f /app/code/bootstrap/cache/*
rm -fr /app/code/storage/framework/cache/data/*
rm -f /app/code/storage/framework/views/*
rm -f /app/code/storage/framework/sessions/*
rm -f /app/code/public/js/builds/*
rm -f /app/code/public/css/builds/*

echo "=> Run migrations"
sudo -E -u www-data php artisan migrate --no-interaction --force

if [[ ! -f /app/data/.inital_admin_created ]]; then
    echo "=> Create initial admin account"

    sudo -E -u www-data php artisan freescout:create-user --role admin --firstName Cloudron --lastName Admin --email admin@cloudron.local --password changeme123 --no-interaction
    touch /app/data/.inital_admin_created
fi

echo "=> Clear cache"
sudo -E -u www-data php artisan freescout:clear-cache
sudo -E -u www-data php artisan config:clear # this clears the config cache under /app/data/bootstrap-cache/config.php which caches old db creds

echo "=> Regenerate vars file"
sudo -E -u www-data php artisan freescout:generate-vars

echo "=> Ensure system email settings"
$MYSQL "INSERT INTO options (name, value) VALUES ('mail_driver', 'smtp') ON DUPLICATE KEY UPDATE value='smtp';"
$MYSQL "INSERT INTO options (name, value) VALUES ('mail_from', '${CLOUDRON_MAIL_FROM}') ON DUPLICATE KEY UPDATE value='${CLOUDRON_MAIL_FROM}';"
$MYSQL "INSERT INTO options (name, value) VALUES ('mail_host', '${CLOUDRON_MAIL_SMTP_SERVER}') ON DUPLICATE KEY UPDATE value='${CLOUDRON_MAIL_SMTP_SERVER}';"
$MYSQL "INSERT INTO options (name, value) VALUES ('mail_port', '${CLOUDRON_MAIL_SMTP_PORT}') ON DUPLICATE KEY UPDATE value='${CLOUDRON_MAIL_SMTP_PORT}';"
$MYSQL "INSERT INTO options (name, value) VALUES ('mail_username', '${CLOUDRON_MAIL_SMTP_USERNAME}') ON DUPLICATE KEY UPDATE value='${CLOUDRON_MAIL_SMTP_USERNAME}';"
$MYSQL "INSERT INTO options (name, value) VALUES ('mail_password', '${CLOUDRON_MAIL_SMTP_PASSWORD}') ON DUPLICATE KEY UPDATE value='${CLOUDRON_MAIL_SMTP_PASSWORD}';"
$MYSQL "INSERT INTO options (name, value) VALUES ('mail_encryption', '') ON DUPLICATE KEY UPDATE value='';"

if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
    echo "=> Pre-setup LDAP"
    $MYSQL "INSERT INTO options (name, value) VALUES ('ldap_account_prefix', 'cn') ON DUPLICATE KEY UPDATE value='cn';"
    $MYSQL "INSERT INTO options (name, value) VALUES ('ldap_admin_account_prefix', 'cn') ON DUPLICATE KEY UPDATE value='cn';"
    $MYSQL "INSERT INTO options (name, value) VALUES ('ldap_base_dn', 'ou=apps,dc=cloudron') ON DUPLICATE KEY UPDATE value='ou=apps,dc=cloudron';"
    $MYSQL "INSERT INTO options (name, value) VALUES ('ldap_encryption', '') ON DUPLICATE KEY UPDATE value='';"
    $MYSQL "INSERT INTO options (name, value) VALUES ('ldap_filter', '(objectCategory=person)') ON DUPLICATE KEY UPDATE value='(objectCategory=person)';"
    $MYSQL "INSERT INTO options (name, value) VALUES ('ldap_host', '${CLOUDRON_LDAP_SERVER}') ON DUPLICATE KEY UPDATE value='${CLOUDRON_LDAP_SERVER}';"
    $MYSQL "INSERT INTO options (name, value) VALUES ('ldap_port', '${CLOUDRON_LDAP_PORT}') ON DUPLICATE KEY UPDATE value='${CLOUDRON_LDAP_PORT}';"
    $MYSQL "INSERT INTO options (name, value) VALUES ('ldap_username', '${CLOUDRON_LDAP_BIND_DN:3:36}') ON DUPLICATE KEY UPDATE value='${CLOUDRON_LDAP_BIND_DN:3:36}';"
    $MYSQL "INSERT INTO options (name, value) VALUES ('ldap_password', '${CLOUDRON_LDAP_BIND_PASSWORD}') ON DUPLICATE KEY UPDATE value='${CLOUDRON_LDAP_BIND_PASSWORD}';"
    $MYSQL "INSERT INTO options (name, value) VALUES ('ldap_register_on_login', '1') ON DUPLICATE KEY UPDATE value='1';"
    $MYSQL "INSERT INTO options (name, value) VALUES ('ldap_server_type', '1') ON DUPLICATE KEY UPDATE value='1';"
    $MYSQL "INSERT INTO options (name, value) VALUES ('ldap_dns', '${CLOUDRON_LDAP_USERS_BASE_DN}') ON DUPLICATE KEY UPDATE value='${CLOUDRON_LDAP_USERS_BASE_DN}';"
    $MYSQL "INSERT INTO options (name, value) VALUES ('ldap_mapping', 'a:3:{s:5:\"email\";s:4:\"mail\";s:10:\"first_name\";s:9:\"givenname\";s:9:\"last_name\";s:2:\"sn\";}') ON DUPLICATE KEY UPDATE value='a:3:{s:5:\"email\";s:4:\"mail\";s:10:\"first_name\";s:9:\"givenname\";s:9:\"last_name\";s:2:\"sn\";}';"
    $MYSQL "INSERT INTO options (name, value) VALUES ('ldap_sync', '1') ON DUPLICATE KEY UPDATE value='1';"
    $MYSQL "INSERT INTO options (name, value) VALUES ('ldap_auth', '1') ON DUPLICATE KEY UPDATE value='1';"
fi

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "=> Configure OIDC"
    OAUTH_PROVIDERS=$(gosu cloudron php <<'EOF'
<?php
    echo addslashes( serialize( [
        [
            'active' => 1,
            'default' => 1,
            'provider' => "oauth",
            'name' => getenv("CLOUDRON_OIDC_PROVIDER_NAME") ?? "Cloudron",
            'id' => "cloudron",
            'client_id' => getenv("CLOUDRON_OIDC_CLIENT_ID"),
            'client_secret' => getenv("CLOUDRON_OIDC_CLIENT_SECRET"),
            'auth_url' => getenv("CLOUDRON_OIDC_AUTH_ENDPOINT"),
            'token_url' => getenv("CLOUDRON_OIDC_TOKEN_ENDPOINT"),
            'user_url' => getenv("CLOUDRON_OIDC_PROFILE_ENDPOINT"),
            'user_method' => "POST",
            'proxy' => "",
            'mapping' => "",
            'scopes' => "openid profile email"
        ]
    ] ) );
EOF
    )

    $MYSQL "INSERT INTO options (name, value) VALUES ('oauthlogin.providers', '${OAUTH_PROVIDERS}') ON DUPLICATE KEY UPDATE value='${OAUTH_PROVIDERS}'"
fi

chown -R www-data:www-data /app/data /run/freescout

echo "=> Start supervisor"
rm -f /run/apache2/apache2.pid
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i freescout
