FreeScout is the super lightweight free open source help desk and shared inbox written in PHP7.
It is a self hosted clone of HelpScout. Now you can enjoy free Zendesk & Help Scout without giving up privacy or locking you into a service you don't control.

***Cloudron user-management is supported through the paid [LDAP module](https://freescout.net/module/ldap/).***

### Features

FreeScout is the perfect helpdesk solution for those who need to provide a professional customer support, but who can not afford to pay for Zendesk or Help Scout.

* No limitations on the number of users, tickets, etc.
* 100% Mobile-friendly.
* Multilingual (English, French, Italian, Portuguese, Russian, Dutch, German, Spanish, Swedish).
* Seamless email integration.
* Starred conversations.
* Push notifications.
* Following a conversation.
* Auto reply.
* Saved replies.
* Notes.
* Email commands.
* Forwarding conversations.
* Moving conversations between mailboxes.
* Sending new conversations to multiple recipients at once.
* Pasting screenshots from the clipboard into the reply area.
* Satisfaction Ratings.
* Configuring notifications on a per user basis.
* Open tracking.
* Tags.
* Editing / hiding threads.
* Search.
* Spam filter.
* Slack integration.
* Translating tickets.
* Time tracking.
* Custom fields.
* White-labeling.
* Custom folders.
* Workflows & SLA.
* Telegram Notifications.
