FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf
RUN a2disconf other-vhosts-access-log && a2dismod perl && a2enmod rewrite env remoteip headers
COPY apache/freescout.conf /etc/apache2/sites-enabled/freescout.conf

RUN crudini --set /etc/php/8.3/apache2/php.ini PHP upload_max_filesize 5G && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP post_max_size 5G && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP memory_limit 512M && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.enable 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.enable_cli 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.interned_strings_buffer 8 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.max_accelerated_files 10000 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.memory_consumption 128 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.save_comments 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.revalidate_freq 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.save_path /run/freescout/sessions && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.3/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.3/cli/conf.d/99-cloudron.ini

# renovate: datasource=github-releases depName=freescout-helpdesk/freescout versioning=semver
ARG FREESCOUT_VERSION=1.8.173

RUN curl -LSs https://github.com/freescout-helpdesk/freescout/archive/${FREESCOUT_VERSION}.tar.gz | tar -xz -C /app/code/ --strip-components 1 -f -

RUN mv /app/code/storage /app/pkg/storage.template && ln -sf /app/data/storage /app/code/storage && \
    mv /app/code/resources/lang /app/pkg/resources-lang.template && ln -sf /app/data/resources-lang /app/code/resources/lang && \
    rm -r /app/code/bootstrap/cache && ln -sf /app/data/bootstrap-cache /app/code/bootstrap/cache && \
    rm -r /app/code/public/css/builds && ln -sf /app/data/public-css-builds /app/code/public/css/builds && \
    rm -r /app/code/public/js/builds && ln -sf /app/data/public-js-builds /app/code/public/js/builds && \
    rm -r /app/code/public/modules && ln -sf /app/data/public-modules /app/code/public/modules && \
    rm -r /app/code/Modules && ln -sf /app/data/Modules /app/code/Modules && \
    ln -sf /app/data/env /app/code/.env && \
    ln -sf /app/data/storage/app/public/ /app/code/public/storage && \
    chown -R www-data:www-data /app/code

# add supervisor configs
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
COPY supervisor/* /etc/supervisor/conf.d/

COPY start.sh cron.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
