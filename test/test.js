#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const EMAIL = 'admin@cloudron.local';
    const PASSWORD = 'changeme123';
    const MAILBOX_EMAIL = 'test@cloudron.io';
    const MAILBOX_NAME = 'cloudrontest';

    var browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function login() {
        await browser.get(`https://${app.fqdn}/login`);
        await browser.findElement(By.id('email')).sendKeys(EMAIL);
        await browser.findElement(By.id('password')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await waitForElement(By.xpath('//div[text()="FreeScout Dashboard"]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//div[text()="FreeScout Dashboard"]'));
        await browser.findElement(By.xpath('//a[@title="Account"]')).click();
        await waitForElement(By.id('logout-link'));
        await browser.findElement(By.id('logout-link')).click();
        await waitForElement(By.id('password'));
    }

    async function createMailbox() {
        await browser.get(`https://${app.fqdn}/mailbox/new`);
        await browser.findElement(By.id('email')).sendKeys(MAILBOX_EMAIL);
        await browser.findElement(By.id('name')).sendKeys(MAILBOX_NAME);
        await browser.findElement(By.xpath('//div[@class="wizard-body"]//button[@type="submit"]')).click();
        await waitForElement(By.id('aliases'));
    }

    async function mailboxExists() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath(`//div[@class="dash-card-content"]//a[text()="${MAILBOX_EMAIL}"]`));
        await waitForElement(By.xpath(`//div[@class="dash-card-content"]//a[text()="${MAILBOX_NAME}"]`));
    }

    // xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    // it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    // it('can get app information', getAppInfo);
    // it('can login', login);
    // it('can create mailbox', createMailbox);
    // it('mailbox exists', mailboxExists);
    // it('can logout', logout);

    // it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    // it('can login', login);
    // it('mailbox exists', mailboxExists);
    // it('can logout', logout);

    // it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    // it('restore app', function () {
    //     const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
    //     execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    //     execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
    //     getAppInfo();
    //     execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    // });

    // it('can login', login);
    // it('mailbox exists', mailboxExists);
    // it('can logout', logout);

    // it('move to different location', async function () {
    //     // ensure we don't hit NXDOMAIN in the mean time
    //     await browser.get('about:blank');
    //     execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    // });

    // it('can get app information', getAppInfo);
    // it('can login', login);
    // it('mailbox exists', mailboxExists);
    // it('can logout', logout);

    // it('uninstall app', async function () {
    //     // ensure we don't hit NXDOMAIN in the mean time
    //     await browser.get('about:blank');
    //     execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    // });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id net.freescout.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login', login);
    it('can create mailbox', createMailbox);
    it('mailbox exists', mailboxExists);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can login', login);
    it('mailbox exists', mailboxExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
