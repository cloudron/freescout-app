[1.0.0]
* Initial version

[1.0.1]
* Fix mail receiving
* Do not recreate admin if manually deleted

[1.0.2]
* Fix translations
* Fix email sending

[1.1.0]
* Update to 1.5.4
* Use new Cloudron base image 2.0
* Support saving replies to original emails in mailbox
* Includes upstream fixes developed by the Cloudron team

[1.1.1]
* Update to 1.5.5

[1.1.2]
* Update to 1.5.6
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.5.6)
* Allow to cancel a queued job (#606, #611)
* Show mailbox name in conversations search.

[1.1.3]
* Update to 1.5.7
* Ability to deactivate module license from Modules page.
* Reply from customer to a deleted conversation should undelete it (#585)
* Do not send auto-replies to own mailboxes.
* Fixed Select all when navigating to the next page (#618)
* Fixed error, when moving conversation to the mailbox where current assignee is not present.
* Fixed sidebar buttons width on mobile.
* Fixed on mobile user icons determining that user is viewing a conversation.
* Sort threads in emails to customers by thread ID instead of created_at (#532)

[1.1.4]
* Update to 1.5.8
* Don't send auto reply for spam emails (#628)
* Vertical scroll for large assignees list (#620)
* Add 1 second delay when sending notifications to users to avoid blocking by sending email service provider.
* Perform second mail send attempt after 5 min, others with 1 hour interval.
* Converted menu.append filter into action.
* Updated NL translations.
* Updated German translations.

[1.2.0]
* Clear the config cache on startup
* Fixup manifest forumUrl

[1.2.1]
* Update FreeScout to 1.5.9
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.5.9)
* Added Conversation History option to conversations under the conversation Settings.

[1.2.2]
* Update FreeScout to 1.5.10
* Added missing time zones.
* Fixed installation wizard for PostgreSQL (#654)
* Fixed fetching emails in PostgreSQL.
* Fixed flatpickr pt-pt.js error in the search.
* Fixed folder conversations number in title.
* Alphabetically sort mailbox list on dashboard (#660)
* Sort mailboxes by name everywhere.
* Reduced scrollable dropdown height a bit.

[1.2.3]
* Update FreeScout to 1.5.11
* Do not show loader when automatically updating conversations (#643)
* Fixed previous conversation button in the Mine folder.
* Updated main menu hooks.

[1.3.0]
* Update FreeScout to 1.5.12
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.5.12)
* Make php.ini customizable
* Fixed showing additional CCs in conversations (#703)
* Fixed factory for 'phones' field of the Customer model (#708)
* Fixed NL translations.
* Check if ORIG_SCRIPT_NAME exists before using it in Helper.php (#699)

[1.3.1]
* Update FreeScout to 1.5.13
* Show emails on Users page.
* Search on Users page.
* Show Message-ID in email logs.
* Rescale large images for readability when viewing conversations.
* Fixed G Suite send error: "Expected response code 250 but got an empty response".
* Updated German translations.
* Improved repeated auto replies detection (#688)

[1.3.2]
* Update FreeScout to 1.5.14
* Added function to save customer photo.
* Show address and country in customer profile if any.
* When creating a phone conversations try to find a customer by phone number instead of creating a new customer.

[1.3.3]
* Update FreeScout to 1.5.15
* Added new hooks to the code for Webhooks Module: https://freescout.net/module/api-webhooks/
* Fixed error on PostgreSQL in RealtimeMailboxNewThread (#731)

[1.4.0]
* Update FreeScout to 1.6.0
* Merge conversations via "Merge" dropdown menu item in the conversation toolbar (#226)
* Edit conversation subject (#43)
* Added Conversation History item to the "Send" button dropdown (#744)
* When viewing a mailbox in the top Search dropdown now "All from current mailbox" item is shown (#591)
* Improved email subject decoding (#735)
* Send full conversation history when forwarding conversation via Workflow (closes #743)
* Added checking availability of shell_exec function and console ps command to System Status page.
* Added curl to the list of required extensions on System Status page.

[1.4.1]
* Update FreeScout to 1.6.1
* Disable built-in update checker
* Mailbox managers with per user settings (#490)
* Printing conversations, messages and notes (#507)
* Import email into multiple mailboxes if needed (#480)
* Properly format Outlook replies (#461)
* Allow translation of search filters.
* Add config variable to disable update checks: APP_DISABLE_UPDATING
* Hide prev-next conversation arrows on a small screen.

[1.4.2]
* Update FreeScout to 1.6.2
* Added proc_open to the list of required functions in System Status.
* Do not cut out regular Gmail quotes (#778)
* Fixed in_array() expects parameter 2 to be array error in BroadcastNotification (#761)
* Hided "New Mailbox" button from non-admins.
* Do no show "Disable User" checkbox to non-admins.
* Run second migration after the module has been activated to avoid possible issues (#773)
* Made permissions manager scrollable on mobile screens (#785)

[1.4.3]
* Update FreeScout to 1.6.4
* Forward conversation to all specified recipients (#825)
* Fixed sql_require_primary_key issue (#818)
* Fixed duplicate image when inserting print screened image (#694)
* Fixed File name too long issue for attachments (#806)
* Check connection only for SMTP send method (#828)

[1.4.4]
* Update FreeScout to 1.6.5
* In user notification use the username part of the email address if customer has no name (#749)
* Add quotes when setting .env variables to avoid error.

[1.4.5]
* Update FreeScout to 1.6.6
* Check and show non-writable cache files in System Status.
* Show number of users on Users page.
* Fixed recipient name on forwarding (#786)

[1.4.6]
* Update FreeScout to 1.6.7
* Check non-writable cache files in the System Status.
* Fixed starring conversations (#662)
* Made foreign keys fields unsigned (#853)

[1.4.7]
* Update FreeScout to 1.6.8
* New fields added to customer profiles.
* Allow to add photos to customers.
* Download button added to attachments.
* Show PHP upload_max_filesize / post_max_size in the System Status.
* Added pagination to customers search.
* Do not fill automatically CC or BCC when forwarding conversations (#877)
* Allow only admin to manage mailbox connection settings (#881)
* Fixed notifications dropdown in German language (#874)
* Do not redirect user to the inaccessible mailbox after moving conversations;
* Do not show deleted users when creating a new mailbox (#866)

[1.4.8]
* Update FreeScout to 1.6.9
* Allow to edit customer messages (#896)
* Show user name when deleting a user.
* Show conversation status icon (closed, deleted, draft, spam) in the search and folders.
* Properly separate replies from FreeScout to FreeScout (#765)

[1.4.9]
* Update FreeScout to 1.6.10
* Highlight new Unassigned and Mine conversations in the dashboard.
* Set "to" in new conversation with url parameter (#802).
* Fixed deactivating licenses.
* Set closed_at if conversation is closed after reply.
* Condensed dashboard content.
* Adjusted logo brightness on hover.

[1.4.10]
* Update FreeScout to 1.16.11
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.6.11)
* Allow to set User Permissions per User (in User Profile > Permissions) (#741)
* Added permission to allow users manage users.
* Support RTL languages with HTML dir attribute in the reply area and conversation threads (#924)
* Automatically delete send logs older 6 months.
* Store user permissions in .env instead of Options to improve performance.
* Hide Delete Conversation button if a user does not have corresponding permissions

[1.4.11]
* Update FreeScout to 1.16.12
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.6.12)
* Do not send notifications on imported conversations (for example via API).
* Fixed the issue when settings in Manage > Settings were not saving instantly due to PHP opcache.
* Fixed customer data covering customer name in the profile.
* Do not show empty To field in the conversation thread.

[1.4.12]
* Update FreeScout to 1.6.13
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.6.13)
* Do not send multiple emails to customers when saving outgoing reply to IMAP folder fails (#984)

[1.5.0]
* Update FreeScout to 1.6.14
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.6.14)
* Update base image to v3
* Optimized Translations page performance.
* Show email header in plain text emails (#730)
* Fixed customer setData issue on fetching (#1013)
* Updated PL, FR and pt-PT translations.
* Added meta column to mailboxes table.

[1.5.1]
* Update FreeScout to 1.6.15
* Knowledge Base Module released.
* Japanese language added.
* Allow to attach files in the mailbox signature.

[1.6.0]
* Make LDAP optionally work. This requiers a paid LDAP module for FreeScout.

[1.6.1]
* Update FreeScout to 1.6.16
* Update base image to v3
* LDAP Module v1.0.13 is compatible with PHP 7.4 now.
* Knowledge Base Module migration issue has been fixed.
* Fixed "Row size too large error" on adding meta column to mailboxes (#1058)
* Fixed invitation email text (#1066)
* Deactivate a module when its folder is renamed.

[1.6.2]
* Update FreeScout to 1.6.17
* Show "New message" alert in the title when new message arrives in the currently open conversation.
* Fixed an issue with sending multiple duplicate emails (#870, #1041)
* Added "gmail_quote" class to the outgoing emails to customers to let other systems (Zendesk) better separate replies.
* Include images links into plain text emails.
* Updated German translation.

[1.6.3]
* Update FreeScout to 1.6.18
* Support for new [Mail Signing & Encryption Module](https://freescout.net/module/mail-signing/)
* Play audio attachments in the browser

[1.6.4]
* Update FreeScout to 1.6.19
* Send full email history when forwarding global settings is set not to include conversation history (#1087)

[1.6.5]
* Update FreeScout to 1.6.20
* New Chat Module released.
* Fixed prefilling "To" field when creating a new conversation.
* Allow to pass UploadedFile as attachment when creating a Thread.
* Allow custom options in fsAjax function.

[1.7.0]
* Update FreeScout to 1.7.0
* Before installing this release make sure to update to the latest versions of all installed modules

[1.7.1]
* Update FreeScout to 1.7.1
* Added extra hooks to conversations table.
* Do not try to send auto reply by email when chat message received (#1151)

[1.7.2]
* Update FreeScout to 1.7.2
* Added meta column to Customers table.
* Block External Images Module released.

[1.7.3]
* Update FreeScout to 1.7.4
* Added State filter to the search to filter Deleted conversations (#912)
* Fixed showing floating alert after sending a reply (#1119)
* Fixed search in PostgreSQL by not using LIKE operator for conversation number and id (#1174).

[1.7.4]
* Update FreeScout to 1.7.5
* Fixed search in PostgreSQL (#1174)
* Process incoming email even if imap_mime_header_decode() fails (#351)

[1.7.5]
* Update FreeScout to 1.7.6
* Added to the Modules page a link allowing to request module license keys by email: https://freescout.net/remind-license-keys/
* Improved createZipArchive() function.

[1.7.6]
* Update FreeScout to 1.7.7
* Added a hook to insert content after mailbox sidebar (#1209)

[1.7.7]
* Update FreeScout to 1.7.9
* Properly update Starred and Drafts folders when moving conversations (#1210)
* Encrypt mailbox SMTP passwords (#328)
* Encrypt system mail SMTP passwords.

[1.7.8]
* Update FreeScout to 1.7.10
* Copy customer email by clicking on it in the sidebar.
* Fix customer sidebar height when it's contend does not fit inside.
* Removed office hours notice from the mailbox auto reply page.
* Updated Polish translation.
* Send Later and Easy Digital Downloads modules released.

[1.7.9]
* Update FreeScout to 1.7.11
* Added Italian translation.
* Fixed Array to string converstion error when creating a customer (#1282)
* Fixed HTML warning on logo image alt attribute.
* Updated Japanese translations.

[1.7.10]
* Update FreeScout to 1.7.12
* Improved accessibility for the visually impaired (#1150)
* Added mailbox name to the title when viewing a mailbox (#1297)
* Added Telegram to the customer social profiles list.
* Fixed duplicating attachments when creating a new conversation (#1186, #1004)
* Fixed open tracking pixel on PHP8 (#1252)

[1.7.11]
* Update FreeScout to 1.7.13
* Make users unique in the Assigned search filter

[1.7.12]
* Update FreeScout to 1.7.14
* Added an arrow showing conversations sorting column (#919)
* Properly restore Cc and Bcc from draft.
* Fixed htmlpurifier INTL_IDNA_VARIANT_2003 issue on PHP7.2 and 7.3 (#1322)
* Fix in_array expects parameter 2 to be array in BroadcastNotification.
* Highlight Unassigned and Mine folders counters.

[1.7.13]
* Update FreeScout to 1.7.15
* Fix saving multiple recipients in a new conversation draft.
* Fix conversation short text in the menu notification.
* Make SVG attachments non-viewable to avoid possible XSS (#1359)

[1.7.14]
* Update FreeScout to 1.7.16
* Allow users to reply from Alternate emails (#1363)
* Fix error when opening a conversation viewed by multiple agents.

[1.7.15]
* Update FreeScout to 1.7.17
* Added extra indexes to Conversations table to improve import performance (#1425)
* Fixed "Undefined constant STDIN" on module activation (#1423)
* Fixed "Call to a member function getFullName" in web_notifications partial (#1410)
* Added SESSION_SECURE_COOKIE parameter to .env.example web installer to improve security of the session cookie.
* Improved security of Translate page.

[1.7.16]
* Update FreeScout to 1.7.18
* Web cron - cron URL can be found in "Manage » System » Cron Commands" (#1452)
* Log error when FreeScout can not fetch message from mail server in Show Original window (#1446)
* Added Danish language.
* Add .eml extension to RFC822 attachments when downloading them (#986)
* Undelete deleted conversation when customer replies via chat (#1468)
* Fixed reopening a conversation when customer replies from chat (#1461)

[1.7.17]
* Update FreeScout to 1.7.19
* GDPR Module released.
* Fixed "unable to run isDraft() on null" error.
* Fixed - customer was empty in the conversation if he/she was deleted and sent a new conversation.
* Minor Dashboard UI updates for more professional look and feel.

[1.7.18]
* Update FreeScout to 1.7.20
* Added conversation.new.customer_sidebar hook on New Conversation page.
* Fixed auto refreshing conversations when new message by user is created (#1405)

[1.7.19]
* Update FreeScout to 1.7.21
* Added Slovak language.
* Allow to set custom module's image via img parameter in modules.json.
* Fixed notifications dropdown height on screen with height > 1086px (#1517)
* Disabled sql_require_primary_key=0 for PostgreSQL (#1518)
* Fixed duplicated customers in the Search (#1521)

[1.7.20]
* Fix logging of client IP

[1.7.21]
* Update FreeScout to 1.7.22
* Properly save attachments without a name (#1548)
* Disabled authentication for cron URL (#1545)
* Fixed creating a phone conversation without saving it first as draft.
* Fixed empty customer when creating a phone conversation without saving it as draft first.
* Fixed (no subject) issue when creating a new conversation and sending it quickly.
* Set data-customer_id on customer conversation page to fix deleting a customer.

[1.7.22]
* Update FreeScout to 1.7.23
* Fixed issue with chat conversations being converted into regular conversations (#1565, #1568)
* Fixed "Trying to get property id of non-object in SendNotificationToUsers.php" (#1557)
* Properly process attachments with slashes (#1566)

[1.7.23]
* Update FreeScout to 1.7.24
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.7.24)
* Settings default subscriptions for new users in Manage > Alerts (#1563)
* Improved accessibility of modal dialogs (#1150)
* Fixed issue of Thread type being incorrect after save.
* Conversations::create() should create phone conversations in Pending status.

[1.7.24]
* Update FreeScout to 1.7.25
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.7.25)
* Update base image to 3.2.0
* Send attachments to all recipients when sending to multiple recipients (#1599)
* Fixed logs names in Logs.
* Removed limitation on user Alternate Emails max length (#1592)

[1.7.25]
* Update FreeScout to 1.7.26
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.7.26)
* Added hooks for custom fetch email providers (#1593).
* Expand customer's address block if it's not empty.

[1.7.26]
* Update FreeScout to 1.7.27
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.7.27)
* SAML Authentication Community Module released.
* Added dashboard_path and home_controller hooks to web routes.

[1.7.27]
* Update FreeScout to 1.7.28
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.7.28)
* Added sorting conversations table by Conversation, Number and Waiting Since columns (#1651)
* Added ability to pass config to Html2Text().
* Fixed "Call to a member function isDraft() on null" in SendNotificationToUsers (#1649)
* Fixed small typo: convsersation -> conversation

[1.7.28]
* Update FreeScout to 1.7.30
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.7.30)
* Fixed note appearing again after sending it (#1434)
* Fixed "Undefined variable: sort_by" (#1662)

[1.8.0]
* Update FreeScout to 1.8.0
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.0)
* Started the process of making FreeScout compatible with PHP 8.1.
* Show merged conversation link in the notice (#1686)
* Fixed redirecting to root in a subdirectory (#1689)
* Fixed customers search on PostgreSQL (#1664)
* Check if mailbox exists in SendReplyToCustomer job (#1678)
* Fixed customer conversations pagination.
* Fixed search pagination with complex search filters.
* Light color for spam conversations (#1680)

[1.8.1]
* Update FreeScout to 1.8.1
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.1)
* Fixed number of issues in 1.8.0 release.

[1.8.2]
* Update FreeScout to 1.8.2
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.2)

[1.8.3]
* Update FreeScout to 1.8.3
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.3)
* Restructured overridden vendor files.
* Flipped reply icon (#1699)

[1.8.4]
* Update FreeScout to 1.8.4
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.4)
* Fixed "Failed opening required compat/vlucas/phpdotenv/src/Loader.php" in the installer and tools.php (#1714)
* Fixed Translation Manager PHP 8 compatibility (#1713)

[1.8.5]
* Update FreeScout to 1.8.5
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.5)
* Improved Module::isActive() method performance: API speed increased 10-30 times.
* Added aria-labels to summernote editor buttons (https://github.com/freescout-helpdesk/freescout/issues/1150#issuecomment-1035104351
* Added aria-labels to select2 options (https://github.com/freescout-helpdesk/freescout/issues/1150#issuecomment-1035104351)
* Fixed php artisan module:make command.
* Do not allow users and mailboxes to have the same email addresses.

[1.8.6]
* Update FreeScout to 1.8.6
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.6)
* Added a template for new pull requests.
* Added hook to adjust mail vars (#1742)
* Fixed parsing dates with underscores in incoming emails (#1738)

[1.8.7]
* Update FreeScout to 1.8.7
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.7)
* Added autocomplete attributes to authentication pages (#1754)
* Added hooks allowing to customize a signature.

[1.8.8]
* Update FreeScout to 1.8.8
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.8)
* Fixed underscores in encoded subjects (#1744)
* If custom login page path set do not show login form at /login (#1751)
* Do not add mailbox emails itself to CC in the new conversation.
* Fixed PHP 8.1 error when opening fetching settings for a new mailbox.
* Allow to save fetching settings with empty password on PHP 8.1
* Fixed issue with searching attachments (#1788)
* Take "Spread the Word" option into account when sending a mailbox test email (#1773)
* Changed homepage link on 404 page (#1762)

[1.8.9]
* Update FreeScout to 1.8.9
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.9)
* Added possibility to connect to Microsoft Exchange Server via oAuth: https://github.com/freescout-helpdesk/freescout/wiki/Connect-FreeScout-to-Microsoft-365-Exchange-via-oAuth
* Fixed homepage URL on 404 page (#1762)
* Fixed saving System Emails page with empty password on PHP 8.1

[1.8.10]
* Update FreeScout to 1.8.10
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.10)
* Added RTL languages support.
* Added Persian language.
* Fixed recipients names when sending new conversations separately (#1802)
* Fixed possible JavaScript error on creating new conversation (#1789)
* Fix PHP 8.1 error in Swiftmailer (#1831)
* Limit Assignee dropdown height in conversations bulk actions (#1840)
* Added empty string check to User::hasManageMailboxAccess() function (#1850)
* Ensure that there is always a Customer email address when updating a conversation (#1847)
* Show extra instructions on error 500 page (#1808)

[1.8.11]
* Update FreeScout to 1.8.11
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.11)
* Allow to translate a message shows after replying (#1862)
* Added filters to be able to filter SMTP settings when sending a mail (#1871)
* Added filter allowing customisation of outgoing headers.
* Fixed Undefined variable error on muting notifications (#1857)
* Fixed Passing null to parameter in Kernel.php (#1884)
* Fixed PHP 8.1 preg_match issue in Mail.php (#1888)

[1.8.12]
* Update FreeScout to 1.8.12
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.12)
* Added a hook to filter outgoing name.
* Added hooks before and after thread recipients.
* Added hooks to allow adding fields to the user profile.
* Added hooks to adjust assignee list.
* Fixed resettings a password on PHP 8.1 (#1911)
* Fixed clearing Fetching logs (#1915)
* Fixed getFullName() on null error after deleting a customer (#1909)
* Trigger a phone call when clicking on a customer phone number (#1899)

[1.8.13]
* Forward the correct client IP through the reverse proxy for logging

[1.8.14]
* Update FreeScout to 1.8.13
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.13)
* Allow to change Default Redirect from mailbox dropdown menu (#1942)
* Added %customer.company% variable to templating.
* Added hooks to add user profile fields (#1923)
* Show error in the log when fetching from custom IMAP folder fails (#1961)
* Do not show separator in the plain text email body for the first thread in the loop (#1927)

[1.8.15]
* Update FreeScout to 1.8.14
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.14)
* Fixed Closed folder sorting (#1965)
* Fixed searching Phone conversations (#1986)
* Fixed typo in update.sh (#1977)
* Fix "Undefined property $conversation" in SendEmailReplyError job (#1974)
* Set SERVER['HTTPS'] variable if needed in HttpsRedirect middleware.
* Improved error message when user replies from random email address (#1971)
* Added $user->setData() method.

[1.8.16]
* Update FreeScout to 1.8.15
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.15)
* Fixed "Call to a member function sortBy() on array" on fetching emails in some cases (#1984)
* Fixed "Canada/Newfoundland" timezone name (#1997)
* Allow passwords with spaces during installation (#1998)

[1.8.17]
* Update FreeScout to 1.8.16
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.16)
* Fixed fetching issue which was created in 1.8.13 release.
* Avoid customer reply duplication loop if "IMAP Folder To Save Outgoing Replies" contains spaces (#2003)

[1.8.18]
* Update FreeScout to 1.8.17
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.17)
* Implemented "IMAP Folder To Save Outgoing Replies" option for oAuth IMAP connection (#2018)
* Added Czech language.
* Added Korean language.
* Added APP_KEY_FILE environment variable (#2022)
* Fixed creating a phone conversation on PHP 8.1 (#2028)

[1.9.0]
* Update FreeScout to 1.8.18
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.18)
* Added basic validation to the customer form (#2034).
* Show phone type in the customer form (#2034).
* Allow to call the customer by clicking on customer's phone number in conversation sidebar (#2034).

[1.9.1]
* Update FreeScout to 1.8.19
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.19)
* Allow to log in in the mobile app when the root / is redirecting to another address (#2039)
* Properly show code blocks when editing threads (#2056)

[1.9.2]
* Update FreeScout to 1.8.20
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.20)
* When emptying trash update starred conversations counter.
* When deleting conversations forever delete them also from conversation_folder table.
* Fixed error in isLocaleRtl() if app.locales_rtl is empty (#2072)
* Rename mailboxes.update_after_signature hook into mailbox.update.after_signature
* Updated apple-touch-icon (#2065)
* Small optimizations to better style dashboard cards and sidebar folders.

[1.9.3]
* Update FreeScout to 1.8.21
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.21)
* Fixed fetching emails via POP3 (#2093)
* Allow to delete mailboxes with unlimited number of conversations (#2089)
* Allow to delete unlimited number of conversations from Trash.
* Fixed open tracking pixel for subdirectory installs (#2112)
* Corrected default customer avatar image URL to work with subdirectory installs (#2121)
* Fixed modules list if some module's author URL is empty on PHP 8.1 (#2127)
* Fixed adding 'Re:' to the subject (#2125)
* Do not log in user automatically after resettings the password to improve the security.
* Updated Spanish translations.

[1.9.4]
* Update FreeScout to 1.8.22
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.22)
* Conversation number now is equal to conversation ID by default. If you want to return to the previous approach, go to Manage > Settings and set Conversation Number option to "Custom" (#2076)
* Check required PHP functions in the web installer (#2187)
* Check fpassthru() function in System Status (#597)
* Added a "Keep Current" option to mailbox Default Assignee and Status After Replying settings.
* Take into account mailbox's Default Assignee option when user replies to the notification by email (#2153)
* Set default empty value for preview in Conversation to avoid errors on PostgreSQL.
* Some changes made for fixes for UI issues for conversation display via x_embed in Kanban (#2141)
* Fixed locking out on login (#2135)
* Fixed decoding binary encoded emails (#2186)
* Show proper error message when trying to forward a conversation without specifying a To address (#2189)

[1.9.5]
* Update FreeScout to 1.8.23
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.23)
* Show bell icon in the subject of conversations you are following.
* Automatically follow a conversation when replying to a conversation assigned to someone else.
* Preserve column sorting when using conversations navigation (#2191)
* Fix searching conversations by number.
* Fix searching convesations in the Merge dialog window.

[1.9.6]
* Update FreeScout to 1.8.24
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.24)
* Make conversation history table wider in the customer profile (#2224)
* Fetch emails by bunches in order to allow fetching of large amounts of emails (#2198)
* Fixed 'Passing null to parameter' in http-foundation/Response.php (#2223)

[1.9.7]
* Update FreeScout to 1.8.25
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.25)
* Allow to set proxy via APP_PROXY variable in .env file (#2242)
* Added iconv to the list of required extensions (#2236)
* Fixed unchecking all default subscriptions (#2247)
* Avoid fatal error when conversation status is zero (#2237)

[1.9.8]
* Update FreeScout to 1.8.26
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.26)
* Allow all users to mute notifications from particular mailboxes.
* Fixed setting a conversation Preview when a conversation is created outside of web interface (#2254)

[1.9.9]
* Enable multiDomain support to allow the knowledge base served from a different domain

[1.9.10]
* Update FreeScout to 1.8.27
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.27)
* Add 'alt' attribute to images in the editor (#2265)
* Add Azerbaijani language to the Translate page (#2269)
* Fixed an issue with the date and encoding of emails added to IMAP Sent folder (#1892)

[1.9.11]
* Update FreeScout to 1.8.28
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.28)
* Include email address of original senders when forwarding a conversation (#1236)
* Allow to forward chat conversations (#2280)
* Fixed grammar: "...and make the application better" (#2278)
* Shortened forward_.. meta fields for mailboxes.

[1.9.12]
* Update FreeScout to 1.8.29
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.29)
* Fixed sporadic Undefined offset: 0 error in reply_fancy.blade.php (#2288)

[1.9.13]
* Update FreeScout to 1.8.30
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.30)
* Preserve table classes when showing messages (#2308)
* Add Greek reply separator (#882)

[1.9.14]
* Update FreeScout to 1.8.31
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.31)
* Set system timezone for new users upon creation (#2312)
* Added 'View Details' button for failed jobs in Manage > System (#2044)
* Hide from Assign list users who are hidden in the mailbox settings (#2327)
* Do not limit line length in chat messages.
* Fixed passing null parameter to Mail::hasVars() (#2335)
* Improved email validation (#2338)

[1.9.15]
* Update FreeScout to 1.8.32
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.32)
* Added support for searching for Unassigned conversations (#2368)
* Allow to prefill the subject via GET parameter when creating a conversation (#2360)
* Do not allow to search conversations and customers in mailboxes to which the user have no access (#2370)
* Updated French translation (#2366)

[1.9.16]
* Update FreeScout to 1.8.33
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.33)
* Show base64-encoded images in conversations (#2355)
* Remove new lines from Subject when saving outgoing email (#2376)
* Do not allow to search conversations and customers in mailboxes to which the user have no access (#2370)
* Do not crop mailbox dropdown menu (#2252)
* Fixed passing null to parameter to nl2br in FetchEmail.php (#2387)

[1.9.17]
* Update FreeScout to 1.8.35
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.35)
* Updated webklex/php-imap lirary to the latest version to fix empty body issue on Microsoft Exchange 365 (#1972)
* Fixed To, Cc, Bcc fields (#2388)

[1.9.18]
* Update FreeScout to 1.8.36
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.36)
* Fixed "Passing null to parameter" in Translate page (#2398)
* Fixed PHP 8.1 error on saving mailbox outgoing settings (#2401)
* Do no show conversations from all mailboxes when the user has no mailboxes assigned (#2409)

[1.10.0]
* Update FreeScout to 1.8.37
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.37)
* Added possibility to clone a conversation from a thread dropdown menu.
* Added ability to sort search results by columns.
* Added extra auto-reply header to the auto reply detection function (#2445)

[1.10.1]
* Update FreeScout to 1.8.38
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.38)
* Jump to the conversation when searching by conversation number.
* Renamed mailbox.before_name hooks.

[1.10.2]
* Update FreeScout to 1.8.39
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.39)
* Fixed an issue when a customer using FreeScout was sending a message and his/her incoming message could sometimes be connected to the wrong conversation (#2375)
* Replace soft hyphens with dash in attachments files to avoid sending issues (#2448)
* Show To dropdown when replying to a conversation with the changed customer (#2455).
* Fixed site.webmanifest 401 error in the browser console if FreeScout is closed with Basic Authentication.
* Show attachment size in the upper case (KB, MB, GB).

[1.10.3]
* Update FreeScout to 1.8.40
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.40)
* Clone attachments when cloning conversations (#2465)
* Updated French translation.
* Prevent note remembering and restoring after sending a note, clicking Back in the browser and clicking "Add Note" (#1434)
* Check if an attachment exists on disk when sending a reply to a customer to avoid sending issues.
* Replace mb_convert_encoding HTML-ENTITIES in FetchEmails (#2461)
* Make javoscript/laravel-macroable-models compatible with PHP 8.2
* Fix preg_replace_callback() issue on PHP 8.1 in Helper::linkify() (#2464)

[1.10.4]
* Update FreeScout to 1.8.41
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.41)
* Updated Polish translations.
* Fixed an error when conversation customer is changed and deleted after that (#2467)
* In SendReplyToCustomer listener do not include messages added after the event has been fired (#2462)
* Added meta column to conversations table.

[1.10.5]
* Update FreeScout to 1.8.42
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.42)
* Fixed sending New Conversation to the customer (#2473)

[1.10.6]
* Update FreeScout to 1.8.43
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.43)
* Fetch forwarded by support agent customer's emails as tickets from customers (#290)
* Added a button to delete all conversations from Spam (#1756)
* Improved PHP 8.2 compatibility.
* Fixed saving editor text after editing the code in the editor (#1705)

[1.10.7]
* Update FreeScout to 1.8.44
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.44)
* Fixed set_user_type_field migration (#2484)
* Allow running workflows in the search when mailbox filter is set (#769)

[1.10.8]
* Update FreeScout to 1.8.45
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.45)
* Improved searching customers by phone numbers.
* Added X-Auto-Response-Suppress header to user notification emails to avoid Exchange out-of-office autoreplys (#2488)
* Added DB_PGSQL_SSLMODE .env variablle allowing to set PostgreSQL SSL Mode(#2485)

[1.10.9]
* Update FreeScout to 1.8.46
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.46)
* Updated Czech translation.
* Fixed passing null to parameter error in SwiftException.php (#2499)
* Improved creating tickets from forwarded emails (#2497)

[1.10.10]
* Update FreeScout to 1.8.47
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.47)
* Allow to convert phone conversations into email conversations by adding email to the customer and replying to conversation.
* Updateed Czech translation.
* Fixed sending emails with attachments on PHP 8.2 (#2505)
* Improved retrieving an email from forwarded email (#2517)
* Fixed translation placeholder in the alert email (#2508)
* Show strikethrough text in conversations (#2504)

[1.10.11]
* Update FreeScout to 1.8.49
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.49)
* Do not require to enter a password when deleting a mailbox if user's password has been generated automatically (#2538)
* Fixed an error when a user is trying to access settings of non-accessible mailbox.
* If --no-interaction flag is passed to schedule:run the script will not run queue:work daemon - implemented specifically for Cloudraon (#2507)
* Run RestartQueueWorker when clearing cache in order to stop queue:work to restart it when the cache is cleared (#2507 (comment))
* Make thread_id parameter optional in Attachment->duplicate().

[1.10.12]
* Update FreeScout to 1.8.50
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.50)
* Allow to add custom headers to outgoing emails via `APP_CUSTOM_MAIL_HEADERS` .env variable (#2546)
* Added freescout:check-requriements command allowing to check console PHP environment.

[1.10.13]
* Update FreeScout to 1.8.51
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.51)
* Fixed an issue with the background job (queue:work) not being executed automatically and executing only after clearing the cache.
* Make sure fetching is executed even if the previous fetch command was killed or threw an error and did not remove the mutex.
* Fix duplicating CC / BCC when restoring a draft of a conversation having CC email(s) (#2555)
* Fixed restoring draft after Undoing the reply when conversation is assigned to the current user.
* Fixed showing the recipient when restoring draft of a forwarded message.
* Fixed CSS minification on PHP 8.2.
* Add small delay between connections when fetching emails to avoid possible mail server connections refusals (#2563)
* Updated French translation.
* Updated Polish translation.
* Automatically clean /tmp folder.

[1.10.14]
* Update FreeScout to 1.8.52
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.52)
* Update number of active conversation in the title when new conversation arrives (#2576)
* Keep spaces after tags when outputting messages content (#2577)
* Made RachidLaasri LaravelInstaller PHP 8.2 compatible.
* Fixed missing vendor/natxet/cssmin/src folder for docker (#2583)
* Improved temp file name generation and cleaning.

[1.10.15]
* Update FreeScout to 1.8.53
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.53)
* Show corresponding message when Show Original window can not load original email via IMAP (#2608)
* Improved Show Original window load speed (#2600)
* Do not distort image proportions when image is scaled using HTML.MaxImgLength during HTML purification (#2599)
* Do not show zero conversations in the title (#2613)
* Made Swift/EmbeddedFile PHP 8.2 compatible.
* Made HTMLPurifier PHP 8.2 compatible (#2623)
* Replace background: with background-color: when purifying HTML (#2560)
* Remove collapse and hidden classes from tables when purifying HTML (#2610)
* Allow to view in a browser only allowed attachment mime types.

[1.10.16]
* Update FreeScout to 1.8.54
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.54)
* Fixed webklex/php-imap issue with email body contained in the "underfined" attachment when using MS365 OAuth IMAP (#1972)
* Fixed determining previous Message-ID (#2086)
* Fixed A non-numeric value encountered error when purifying HTML (#2629)

[1.10.17]
* Update FreeScout to 1.8.55
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.55)
* Fixed sending reply to the customer when replying to the phone conversation (#2627, #2637)
* Fixed /img/loading.gif not found (#2644)
* Fixed original message not always being fetched in Show Original window (#2638)
* Fixed Undefined variable $result error on fetching original message (#2639)
* Change extension if uploaded PDF file contents JavaScript.
* Remove script tags from uploaded SVG images.
* Escape modal titles.

[1.10.18]
* Update FreeScout to 1.8.56
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.56)
* Added extra options to Fetch Mail Schedule in Manage > Settings > Mail Settings (#2489)
* Import emails sent to multiple mailboxes via Bcc (#2652)
* Set timeouts for email sending jobs to avoid queue:work getting stuck.
* Fixed deprecation errors in Illuminate/Config/Repository.php (#2636)
* Do not run multiple App\\Jobs\\RestartQueueWorker jobs when restarting queue:work.

[1.10.19]
* Update FreeScout to 1.8.57
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.57)
* Fixed some bugs when making a conversation Unassigned when deleting a user (#2660)
* Fixed some bugs when sending emails separatately to recipients (#2661)
* Fixed fetching emails to multiple mailboxes at once via Cc (#2659)
* Fixed Error occured error when saving draft of a phone conversation without a customer.
* Decrease Starred folder counter when discarding starred draft.
* Fixed populating the subject when creating a New Conversation from existing message (#2662)

[1.10.20]
* Update FreeScout to 1.8.58
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.58)
* Send alerts to all admins instead of just the first one (#2686)
* Save attachments when saving outgoing emails into IMAP Sent folder via "IMAP Folder To Save Outgoing Replies" option (#2699)
* Add attachment paperclip to the conversation when merging conversations with attachments (#2681)
* Made link popover disappear when the editor looses focus (#2678)
* Fixed showing dates in Portuguese language (#2670)
* Fixed a bunch of typos (#2677)

[1.10.21]
* Update FreeScout to 1.8.59
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.59)
* Added theme-color meta filter (#2719)
* Set timeouts for curl and GuzzleHttp requests.
* Do not detect image src as a customer when fetching email forwarded by a support agent (#2672)
* Replace new lines with spaces in conversation previews (#2711)
* Fixed Newer conversation button (#2716)

[1.10.22]
* Update FreeScout to 1.8.60
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.60)
* Fixed passing null to strtr() in Helper::textPreview() (#2720)

[1.10.23]
* Update FreeScout to 1.8.61
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.61)
* Show extra instructions when invitation email to user can not be sent (#2730)
* Delete records from followers table when conversation is deleted (#2732)
* Fixed an issue when a module could not be updated due to existing Public symlink (#2709)
* Fixed moving conversations to the customer when merging phone customers (#2722)

[1.10.24]
* Update FreeScout to 1.8.62
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.62)
* Fixed "Send and stay on the page" option when creating a new conversation.
* Take into account proxy settings in Helper::downloadRemoteFile() function (#2755)
* Do not add Re: to the email saved in IMAP folder when creating a new conversation in FreeScout or forwarding a conversation (#2509)
* Do not add Re: to the email subject when a forwarding conversation in FreeScout (#2759)
* Do not convert phone conversation into email conversation when adding a note (#2766)

[1.10.25]
* Update FreeScout to 1.8.65
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.65)
* Fixed "Undefined array key argv" error.
* Automatically kill fetch-emails command running for too long (#2489, #2762)
* Fixed undefined variable $to on fetching forwarded emails (#2771)
* Fixed creating a new conversation with "Send and go to next active" option enabled (#2769)

[1.10.26]
* Update FreeScout to 1.8.66
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.66)
* Added Finnish language.
* Updated Czech translation.
* When deleting an email from a customer do not create a new customer if there are no conversations with such email (#2767)
* Fixed attachment names encoded in windows-1258 in Webklex/laravel-imap library (#2782)
* Fixed "Mailbox name is not valid mUTF-7" error in Webklex/laravel-imap (#2793)
* Improved retrieving original message with Wekblex/php-imap library.
* Fixed a bunch of issues in Webklex/php-imap library.

[1.10.27]
* Update FreeScout to 1.8.67
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.67)
* Show an alert in the mailbox to all users if there are problems with outgoing mail queue processing (#2808)
* Detect incoming message sent from a mailbox as a reply when original email sent to multiple mailboxes (#2807)
* Send alerts to activated admins only.
* Clean /storage/app/updater folder before installing a new FreeScout version to avoid installing some random release.

[1.10.28]
* Update FreeScout to 1.8.68
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.68)
* Updated Czech translation.
* menu.mailbox... hooks added (#2816)
* Properly show an original message even if an email is moved after fetching to another IMAP folder (#2820)
* Use curl instead of file_get_contents() to load remote files to avoid errors on some hostings.
* Allow single backslashes in .env file parameters (#2822)
* Create vendor/natxet/cssmin/src folder in composer.json for Docker (#2583)
* Do not use Reply-To as a sender email for auto-responders (#2826)
* Fixed popover z-index (#2829)
* Update conversation preview when deleting a note (#2832)
* Fixed typos (#2812)
* Substitute a customer in the forwarded email only if there is @fwd text added to the beginning of the email (#2813)

[1.10.29]
* Update FreeScout to 1.8.69
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.69)
* Fixed an empty body issue when fetching emails with modern Microsoft OAuth IMAP authentication via Webklex/php-imap library (#1972)

[1.10.30]
* Update FreeScout to 1.8.70
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.70)
* Added a Retry button allowing to retry background jobs in Manage > System (#2881)
* Added Norwegian language.
* Fixed selecting conversations via long press in mobile app (#2868)
* Revert conversation preview after undoing sending a reply (#2874)
* Fixed a typo in freescout:check-requirements command.
* Improved Filesystem.php security on Windows.
* Changed queue:work check interval to avoid false alarms (#2876)
* Take into account proxy settings from .env in Helper::getRemoteFileContents().
* Escape link text when inserting a link in the editor.
* Always open links in conversations in a new tab.

[1.10.31]
* Update FreeScout to 1.8.71
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.71)
* Updated Brazilian, Polish and Italian translations.
* Improved PHP version check in tools.php (#2901)
* Renamed menu hook into menu.mailbox_single.after_name.
* Pass threads_count to reply_email.before_signature and reply_email.after_signature hooks (#2908)

[1.10.32]
* Update FreeScout to 1.8.72
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.72)
* Allow to reply from Mailbox Aliases (#656)
* Allow to change customer photo size via `APP_CUSTOMER_PHOTO_SIZE` .env variable (#2919)
* Allow to change user photo size via `APP_USER_PHOTO_SIZE` .env variable (#2919)
* Add Norwegian translation.

[1.10.33]
* Update FreeScout to 1.8.73
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.73)
* Updated Czech translation.
* Fixed replying to email notifications by support agents (#2941)

[1.10.34]
* Update FreeScout to 1.8.74
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.74)
* Show dates translated into selected language (#2950)
* Allow to select mailbox alias in From field when creating a new conversation (#656)
* Fix wrong messages order in the email to customer in some cases (#2938)
* Do not show From (Alias) field when creating a phone conversation.
* Clean temp files daily instead of weekly (#2949)

[1.10.35]
* Update FreeScout to 1.8.75
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.75)
* Automatically load new notes when viewing a conversation (#2961)
* Restart swiftmailer after each sent email to remove temp files (#2949)
* Added .env option allowing to enable updating folders counters in background (#2982)

[1.10.36]
* Update FreeScout to 1.8.76
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.76)
* Added "Allow to reply from aliases" option to mailboxes (#2988)
* Fixed showing mailbox mute status for non-admin users (#2992)
* Improved email subject decoding (#2987)
* Improved background to background-color CSS style conversion in conversations (#2969)
* Improved displaying tables in conversations (#2969)
* Improved sanitizing SVG files.
* Limit "Insert variable" dropdown width to avoid issues in Firefox (#2966)
* Fixed "Passing null" error in Manage > Settings > Mail Settings (#2989)

[1.10.37]
* Update FreeScout to 1.8.77
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.77)
* Added customer.deleting hook (#3003)
* Fixed updating custom folders counters (#2998)
* Properly thread replies from Jira into conversations (#2927)
* Fixed minutes formatting in 12-hour clock mode (#2997)
* Automatically unfollow followed conversation when it is assigned to the user (#3001)

[1.10.38]
* Update FreeScout to 1.8.78
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.78)
* Allow to set `same_site` parameter to none in config/session.php (#3023)
* Updated German translation.
* Allow to save unlimited number of recipients in new conversation draft (#3036)
* Fixed saving OpenOffice attachments (#3048)
* Do not show attachment URLs in printed version (#3035)
* Show links hrefs only in thread content in the printed version (#3045)

[1.10.39]
* Update FreeScout to 1.8.79
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.79)
* Updated German translation.
* Replace base64 images in replies with attachment URLs (#3057)
* Do not try to send email again if mail server replies with "message file too big" error (#3065)
* Fix subject with colon being truncated in Webklex/php-imap library (#2964)
* Translate filter names in the the Search (#3058)
* Fixed "Undefined array key 0" error in Subscription class (#3059)
* Fixed module activation issue when /public/modules symlink exists but leads to wrong path.
* Improved subject decoding with iconv_mime_decode() function (#3066)
* Fixed error on Users page when user's first name is empty (#2972)
* Remove app.force_https parameter and determine protocol from app.url parameter instead (#3053)
* Decode subjects using iconv_mime_decode() function in Webklex/php-imap library (#3039)

[1.10.40]
* Update FreeScout to 1.8.80
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.80)
* Fixed an issue with HTTPS redirect introduced in the previous release (#3075, #3070)

[1.10.41]
* Update FreeScout to 1.8.81
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.81)
* Improved subject decoding (#3089)
* Improved email validation: do not allow @unknown emails (#3101)
* Improved body decoding on fetching (#3089)
* Improved decoding attachment names with Webklex/PHP-IMAP library (#2753)
* Fixed decoding sender name in iso-2022-jp encoding (#3089)

[1.10.42]
* Update FreeScout to 1.8.82
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.82)
* Updated Dutch translation.
* Preserve new lines when replacing vars in the signature (#3146)
* Fixed saving data into Timestamp field on PostgreSQL.
* Improved decoding strings and subject in Webklex/laravel-imap library.
* Show proper error message when module license key is invalid instead of "Module not found in the modules directory".
* Made using imap_utf8_to_mutf7() function optional in Webklex/laravel-imap library (#3152)

[1.10.43]
* Update FreeScout to 1.8.83
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.83)
* Fixed the issue with retrieving attachments introduced in the previous release (#3154)

[1.10.44]
* Update FreeScout to 1.8.84
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.84)
* Check pcntl extension status in System to avoid "Undefined constant SIGKILL" error.
* Fixed PHP version check in tools.php (#3162)
* Fixed GuzzleHttp\Psr7\Uri::withScheme() error on PHP 7.1 (#3158)
* Do not try to resend email notifications for received bounce emails (#3156)
* Do not encode HTML entities in outoging plain text emails (#3159)
* Improved subject decoding (#3167)

[1.10.45]
* Update FreeScout to 1.8.85
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.85)
* Improved iso-2022-jp subjects and attachment names decoding (#3185)
* List all Message-IDs in References header in replies to customers (#3175)
* Fixed "Attempt to read property id on null" in reply_fancy (#3182)
* Improved suject decoding (#3177)
* Fixed undefined variable bug in FollowerObserver (#3172)
* Limit the number of displayed non-writable cache files on Status page (#3165)
* Do not create an empty conversation when one email is being fetched by two processes at the same time (#3186)
* Updated Russian translation.

[1.10.46]
* Update FreeScout to 1.8.86
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.86)
* Allow to set trusted proxies via .env file using APP_TRUSTED_PROXIES parameter to allow proper client IP detection when using balancers, proxies or CloudFlare (#1994)
* Do not delete the message when another user discards a draft of the reply which just has been sent (#2873)
* Fixed "References" header in emails to customers (#3175)

[1.10.47]
* Update FreeScout to 1.8.87
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.87)
* Check pcntl extension in the console version of PHP on Status page (#3190)
* Improved subject decoding (#3196)
* Save CC and BCC when saving outgoing email to the Sent IMAP folder (#3228)
* Make sure that conversation subject has String type when fetching emails.
* Save ICS attachments having no name as calendar.ics (#1412)
* Allow to view .json, .diff and .patch attachments (#3192)

[1.10.48]
* Update FreeScout to 1.8.89
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.89)
* Fixed `shell_exec()` error on Status page introduced in the previous release (#3239)
* Fixed an issue with the customer reply being connected to the wrong conversation in some cases (#3231)

[1.10.49]
* Update FreeScout to 1.8.90
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.90)
* Allow to use user variables in mailbox Custom From Name (#1031)
* Improved subject and attachment names decoding (#3248)

[1.10.50]
* Update FreeScout to 1.8.91
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.91)
* Optimized Eventy hooks code - in some cases pages now load 30% faster (#3263)
* Updated Polish and Czech translations.

[1.10.51]
* Update FreeScout to 1.8.93
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.93)
* Added Chinese Simplified language.
* Added a .env file parameter allowing to restrict users to see only tickets assigned to themselves (#701)
* Updated Danish translation.
* Added findCustomersBySocialProfile() function.
* Added customer_channel table.
* Show an error message to support agent if an emial has not been sent after 1 hour - while FreeScout continues to try to send it (#3268)
* Fixed an issue with Unassigned and Assigned folders introduced in the previous release (#3287)

[1.10.52]
* Update FreeScout to 1.8.94
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.94)
* Remove automatically old records from notifications table (#3294)
* Fixed an issue with importing modules translations occurring in some cases (#3289)

[1.10.53]
* Update FreeScout to 1.8.95
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.95)
* Fixed an issue with the editor adding new lines when pasting HTML (#3264)
* Added "Mail Date and Time" option allowing to use date from mail headers when fetching emails (#3297)
* Added page parameter to URL when navigating over conversations (#3323)
* Replace non-replaced vars in mailbox custom from name with empty string (#1031)
* Fixed migration creating customer_channel table (#3309)
* Improved mail subject decoding (#3312)
* Fixed "Invalid text representation" error on PostgreSQL when saving mailbox permissions (#3315)
* Show correct 12 or 24 hour time format in calendars (#3322)

[1.10.54]
* Update FreeScout to 1.8.96
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.96)
* Show in Outgoing Emails the SMTP Queue ID for emails passed for delivery (#3330)
* Show link to the conversation and to logs in SendReplyToCustomer jobs on System Status page (#3331)
* Show DB info and missing migrations on System Status page (#3326)
* Check on System Status page if symlink() function is available (#3335)
* Set CURLOPT_SSL_VERIFYPEER option to false for curl and Guzzle requests (#3293)
* Updated Spanish translation (#3343)
* Automatically check invalid or missing modules symlinks and show error messages on Modules and System Status pages.
* Fixed variable name in saveUserThread() function on fetching replies to user email notifications (#3325)
* Fixed an issue with "Allow to reply from aliases" option being reset when mailbox settings saved by non-admin user (#3337)
* Cancel SendReplyToCustomer background jobs when undoing replies (#3300)
* Do not insert new line in the editor when pasting content (#3319)
* Move star mark when merging conversations (#3351)
* Show SMTP 5xx errors in the conversation immediately (#3339)

[1.10.55]
* Update FreeScout to 1.8.97
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.97)
* Automatically fix module symlink when module Public folder is a broken symlink.
* Fixed an error on System Status page

[1.10.56]
* Update FreeScout to 1.8.98
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.98)
* Fixed "No query results for model error" on Status page (#3366)
* Fixed error on Status page when "open_basedir restriction in effect" occurs (#3375)
* Set default CURLOPT_CONNECTTIMEOUT value for curl.

[1.10.57]
* Update FreeScout to 1.8.99
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.99)
* Improved attachment names sanitizing (#3377)
* Allow to update all modules at once.
* Added freescout:module-update command allowing to update all modules at once (#3384)
* Added freescout:module-update command and --yes flag to tools/update.sh script (#3384)
* Created GitHub continuous integration test.
* Properly process situation when application can not parse mail Date header on fetching (#3394)
* Removed "Mail Date and Time" option from Mail Settings (#3380)

[1.10.58]
* Update FreeScout to 1.8.100
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.100)
* Improved conversation accessibility (#3415)
* Improved dependencies security.
* Added fetch_emails.mailbox_to_save_message hook (#3408)
* Fixed the issue due to which long fetching may be killed before it finishes (#3417)
* Fixed discarding new phone conversation draft (#3407)

[1.10.59]
* Update FreeScout to 1.8.101
* Update base image to 4.2.0
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.101)
* Improved uploads folder security (#3428)
* Updated German translation.
* Created a CI test for testing the app with PostgreSQL.
* Fixed an error in PostgreSqlPlatform.php on PHP 8.2 with PostgreSQL.

[1.10.60]
* Update FreeScout to 1.8.102
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.102)
* Enable Content Security Policy (CSP) - [read more here](https://github.com/freescout-helpdesk/freescout/issues/3443)
* Save base64 data images as attachments on fetching.
* Set proper embedded flag for attachments corresponding to images embedded into incoming emails.
* Do not add paperclip icon to conversations when email contains only embedded images.
* Do not show embedded images as attachments in conversations.

[1.10.61]
* Update FreeScout to 1.8.103
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.103)
* Fixed CSP error on module install button click

[1.10.62]
* Update FreeScout to 1.8.104
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.104)
* If you are using Apache you'll need to install headers module.
* New block for nginx config has to be inserted in the very specific place of the nginx config (see instruction).
* filename=$1 changed to filename=$2 in the new block for the nginx config.
* Added an icon to chat conversations.
* Fixed Apache "headers module not found" error for attachments (#3450)
* Updated nginx config in install.sh.

[1.10.63]
* Update FreeScout to 1.8.105
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.105)
* Added X-Frame-Options header preventing embedding FreeScout via iframe.
* Added "Chat Mode" for Chat conversations.
* Allow to set APP_CLOUDFLARE_IS_USED=true in the .env file for proper client IP detection when CloudFlare is used (#3467)
* Fixed an issue when support agent replies to the email notifications and the conversation is moved to another mailbox (#3455)
* Improved converting URLs into clickable links (#3464)
* Allow more attributes when purifying HTML (#3463)
* Added client_max_body_size 20M to nginx config in install.sh (#3453)

[1.10.64]
* Update FreeScout to 1.8.106
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.106)
* Added "Max. Message Size" option in "Manage » Settings" (#3479)
* Fetch emails into several FreeScout mailboxes when customer replies to multiple mailboxes at once (#3473)
* Fixed an issue with multiplying attachments when forwarding a conversation and discarding its draft.
* Fixed overriding of viewable attachments via APP_VIEWABLE_ATTACHMENTS parameter in .env file (#3481)
* Show "All from current mailbox" link in the Search dropdown in conversations (#3480)

[1.10.65]
* Update FreeScout to 1.8.107
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.107)
* Updated Polish translation.
* Fixed incorrect redirect when replying to the last active conversation (#3486)
* Set max length for user attribtues before saving a user to avoid "String data right truncated" error on PostgreSQL (#3489)
* Improved font color sanitizing when purifying thread HTML (#3492)
* Fixed installing helpdesk without HTTPS via install.sh script.
* Dynamically expand "Logs Monitoring" options in "Manage » Alerts" settings.

[1.10.66]
* Update FreeScout to 1.8.108
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.108)
* Fixed introduced in the previous release error with "Passing null to parameter error" in User.php when creating users outside of the web interface (#3501)
* Fixed an issue with phone customers always shown in the search (#3499)
* Fixed fetching emails from non-ASCII imap folders (#3502)
* Fixed saving outgoing replies to the IMAP Sent folder with non-ASCII name.
* Added html5sortable.js script to the core.

[1.10.67]
* Update FreeScout to 1.8.109
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.109)
* Check pcntl_signal() function on System Status page (#3515)
* Allow to set SMTP timeout in seconds (default is 30 seconds) via .env parameter: MAIL_SMTP_TIMEOUT=120
* Allow to retry thread sending from a conversation (#3517)
* Fixed a cicle between two tickets with Older/Newer buttons (#3506)
* Fixed "Undefined array key 1" error with Webklex/PHPIMAP library (#3503)
* Fixed displaying SVG images in the uploaded folder on Apache web server (#3522)
* Fixed viewing attachments having percents in their names (#3530)
* Mark email as read when saving to IMAP Folder with Webklex/PHPIMAP library (#3533)
* Removed max length limitation from mailbox's Aliases field (#3525)

[1.10.68]
* Update FreeScout to 1.8.110
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.110)
* Dynamically load customer info when creating a new conversation (#2402)
* Allow to select multiple conversations using SHIFT button (#1312)
* Updated Portuguese translation.
* Merge multiple conversations at once (#3516)
* Check pcntl_signal() function on System Status page in the console PHP.
* Improved linkify algorithm (#3402)
* Autosave new conversation draft when choosing a recipient (#3538)
* Make error message in logs more descriptive when SendReplyToCustomer job can not save outgoing reply to the IMAP folder.
* Fixed "Select None" button (#3546)

[1.10.69]
* Update FreeScout to 1.8.111
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.111)
* Fixed "Call to undefined function shell_exec()" on Status page (#3570)
* Improved linkify algorithm (#3402)
* Do not change conversation customer when CCed email replies to a conversation (#3551)

[1.10.70]
* Update FreeScout to 1.8.112
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.112)
* Fixed an issue with the app version introduced in the previous release.

[1.10.71]
* Update FreeScout to 1.8.113
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.113)
* Allow to translate English texts (#3599)
* Show a notice with instructions on System Status page if CloudFlare is used.

[1.10.72]
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.114)
* Update FreeScout to 1.8.114
* Fixed email sending loop issue (#3607)
* Do not strip HTML tags in plain text emails (#3614)
* Add as CC a customer who was in CC and replied to the conversation (#3613)

[1.10.73]
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.115)
* Update FreeScout to 1.8.115
* Allow to fetch emails from nested folders (#3623)
* Fixed "Packets out of order" error (#3619)
* Fixed "Trying to access array offset on value of type null" error in `locale_options` template (#3620)
* Added StartTLS encryption option on Fetching Emails page when new Webklex/php-imap library is used (#3624)
* Improveed linkify algorithm.

[1.10.74]
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.117)
* Update FreeScout to 1.8.117
* Localize calendar
* Fix "Call to undefined method getHashedRelySeparator()" error introduced in the previous release.

[1.10.75]
* Update FreeScout to 1.8.118
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.118)
* Updated German and Slovak translations.
* Improved HTML purification (#3664)
* Fixed counter not being aligned in Dashboard (#3665)
* Made summernote editor compatible with jQuery v3.6.0 (#3666)
* Made html5sortable.js work on mobile devices (#3671)
* Do not show Delete button in bulk actions if user does not have corresponding permissions (#3680)
* Fixed "Use of undefined constant FT_UID" in Webklex/laravel-imap (#2604)

[1.10.76]
* Update FreeScout to 1.8.119
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.119)
* Fixed main menu issue on mobile devices introduced in the previous release (#3724)
* Fixed conversations list scrolling issue on mobile devices introduced in the previous release (#3724)
* Fixed "Trailing data" error on PostgreSQL (#3702)
* Fixed an error in HTMLPurifier/Length.php (#3721)

[1.10.77]
* Update FreeScout to 1.8.120
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.120)
* Updated Italian translation
* Fixed an issue with multiple emails being sent to customers when "IMAP Folder To Save Outgoing Replies" is set for a mailbox (#3632)
* Fix "Rredeclaration of let DragDropTouch" error on Translate page (#3717)
* Show "Recipient is required" message in red color.

[1.10.78]
* Update FreeScout to 1.8.121
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.121)
* Allow to set session.same_site via SESSION_SAME_SITE .env parameter (#3023)
* Improved searching customers by phone number when creating Phone conversations (#3786)
* Updated Italian translation (#3755)
* Added Turkish language.
* Improved sorting elements on mobile devices (#3800)
* Removed APP_URL from minify.config.php to avoid CORS issues on custom domains.
* Fixed showing scaled image in conversations (#3798)
* Allow style attribute for tr in threads HTML (#3799)
* Removed 255 symbols limit from mailbox Alises input.

[1.10.79]
* Update FreeScout to 1.8.122
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.122)
* Fixed an issue with CSS and JS paths on some installations introduced in the previous release (#3818)

[1.10.80]
* Update FreeScout to 1.8.125
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.125)
* Fixed a word+colon mistakenly converted to URL in the editor (#3822)
* Fixed "Numeric value out of range" error in the search on PostgreSQL (#3831)
* Fixed creating conversations by a user seeing only assigned conversations (#3843)
* If a user can see only assigned conversations, send him notifications only for assigned to him conversations (#3843)
* Fixed adding notes to Spam conversations.
* Fixed "Passing null to parameter" error in Helper::stripTags() (#3849)
* Do not save SMTP username and password in Send Log status message.
* Improved accessibility of exact date in conversation table (#3829)
* Show module author on Modules page (#3851)
* Allow updates for third party modules (#3851)
* Fixed an issue with Closed folder introduced in the previous release (#3858)

[1.10.81]
* Update FreeScout to 1.8.126
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.126)
* Fixed an issue with updating modules introduced in the previous release.
* Disable "Undo" reply feature for chat conversations in the Chat Mode (#3856)

[1.10.82]
* Update FreeScout to 1.9.127
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.127)
* Show extended editor toolbar when editing threads (#3866)
* Properly process a situation when an attachment can not be saved to the disc (#3871)
* Preserve elements inside [if mso] when purifying thread content (#3865)
* Remove [if mso] from thread content when purifying (#3865)
* Allow <center> element when purifying thread content.
* Use latest PHP version (8.3) for linting (#3864)
* Upgraded DataTables version to v1.13.11.

[1.10.83]
* Update FreeScout to 1.9.128
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.128)
* Fixed long running fetch commands being killed (#3844)
* Fixed conversation bulk actions issue after sorting conversations (#3884)
* Fixed conversations bulk actions when page parameter is set in the URL.
* Fixed "Passing null to parameter" error in HTMLPurifier (#3893)
* Fixed removing comments when purifying HTML (#3894)
* Strip script tags from mailbox signature.

[1.10.84]
* Update FreeScout to 1.9.130
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.130)
* Updated Japanese translation.
* Fixed "conv_type_custom" JS error introduced in the previous release (#3923)
* Use encrypted attachment IDs when uploading attachments.
* Added code allowing to add Custom conversation types via modules (#3918).
* Updated Japanese translation.
* Add sender name to the From header when saving emails to the Sent folder (#3900)
* Fixed deleting conversations after sorting them (#3901)

[1.10.85]
* Update FreeScout to 1.9.131
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.131)
* Allow fetching seen/read messages via scheduler (#3931)
* Updated Polish translation.
* Allow limiting freescout:fetch-emails command to specific mailboxes (#3932)
* Fixed retrieving IMAP subfolders (#3933)
* Fix To and From headers when saving outgoing emails to the Sent IMAP folder (#3900)
* Fixed saving into Sent IMAP folder messages with attachments (#3934)

[1.10.86]
* Update FreeScout to 1.8.132
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.132)
* Updated Russian translation (#3945)
* Fixed "Passing null" error in symfony/http-foundation/Request.php (#3940)
* Fixed duplicated folder names when retrieving IMAP folders (#3937)
* Fixed "Call to a member function getFullName() on null" error in the notification template (#3942)
* Fixed an issue with some emails shown incorrectly - introduced in 1.8.127 release (#3907)

[1.10.87]
* Update FreeScout to 1.8.133
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.133)
* Updated Polish translation.
* Do not mark a thread as containing attachments when only embedded images are present in the email.
* Fixed attachments being duplicated when discarding a message forwarding draft (#3949)
* Fixed attachments not being sent when forwarding a conversation (the issue introduced in 1.8.131 release).
* When forwarding a conversation do not include attachments from existing in the conversation thread drafts.
* Fixed an issue with removing attachments when replying to the conversation (the issue introduced in 1.8.131 release).
* Fixed To field not appearing after discarding a message forwarding draft (#3950)
* Fixed an error when new conversation draft is being discarded by another user (#3951)
* Updates and improvements made in Webklex/php-imap library.

[1.10.88]
* Update FreeScout to 1.8.135
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.135)
* Fixed "Header::decode() is not supported" error in Webklex/php-imap (#3960)
* Fixed "Parse error" in Attachment.php on fetching email attachments (#3965)

[1.10.89]
* Update FreeScout to 1.8.136
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.136)
* Updated Dutch translation.
* Fixed attachments not being shown when reopening a new conversation draft (#3957)
* Do not run scheduled commands when running `\Artisan::call()` function - on System and Modules pages (#3970)

[1.10.90]
* Update FreeScout to 1.8.140
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.140)
* Fixed browser push notifications being sent not to all users (#3880)
* Do not show Closed conversations in non-standard folders (#3988)
* Fixed "Unknown hashing algorithm: crc32c" error in Webklex/php-imap library (#3991)
* Fetch emails sent to multiple mailboxes on a per mailbox basis (#3941)

[1.10.91]
* Update FreeScout to 1.8.138
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.138)
* Fixed cron jobs not being run via URL - issue introduced in v1.8.136 release (#4000)

[1.10.92]
* Update FreeScout to 1.8.139
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.139)
* Updated Polish translation.
* Fixed CC and BCC fields when cloning a conversation (#4011)
* Do not allow JavaScript Prototype Pollution via getQueryParam() function (GHSA-rx6j-4c33-9h3r)
* Remove unwanted tags from HTML when editing threads (GHSA-985r-6qfc-hg8m)
* Strip unwanted tags from mailbox signature when saving it.
* Strip unwanted tags from mailbox Auto Reply.

[1.10.93]
* Update FreeScout to 1.8.140
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.140)
* Switch to the new fetching library (Webklex/php-imap).

[1.11.0]
* Update FreeScout to 1.8.141
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.141)
* Update base image to 5.0.0
* Fixed an issue with the reply not being connected to the conversation in some cases (#4026)
* Fixed retrieving the original message in Show Original window for moved conversations.
* Fixed fetching emails forwarded using @fwd command (#4036)
* Fixed IMAP folders not being retrieved in some cases.
* Skip auto-replies sent to the email notification on behalf of a user (#4035)

[1.11.1]
* Update FreeScout to 1.8.142
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.142)

[1.11.2]
* Update FreeScout to 1.8.143
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.143)

[1.11.3]
* Update FreeScout to 1.8.144
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.144)

[1.11.4]
* Update FreeScout to 1.8.145
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.145)

[1.11.5]
* Update FreeScout to 1.8.146
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.146)

[1.11.6]
* Update FreeScout to 1.8.147
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.147)

[1.11.7]
* Update FreeScout to 1.8.148
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.148)
* Execute hooks when performing conversations bulk deleting to Deleted folder (#4141)
* Correctly process invalid mail Reply-To headers (#4158)
* Correctly processs invalid dates when fetching emails (#4159)
* Leave other emails unread when an exception happens during fetching of one of them (#4159)
* Make toSql() comparison compatible with PostgreSQL (#4167)
* Prefill SMTP username and password for MS365 if those fields are empty or if set to outlook.office365.com (#4151)
* Log Helper::getRemoteFileContents() errors to App Logs.

[1.11.8]
* Update FreeScout to 1.8.149
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.149)
* Populate variables in custom Mailbox names in emails sent to customers (#4169)
* Improve Webklex/php-imap memory usage (https://github.com/Webklex/php-imap/issues/433#issuecomment-2288560006)
* Do not add quotes around the date in IMAP SINCE query (#4175)

[1.11.9]
* Update FreeScout to 1.8.150
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.150)
* Fixed fetching issue introduced in the previous release (#4182)
* Allow to disable quotes around SINCE date in IMAP search query via `APP_SINCE_WITHOUT_QUOTES_ON_FETCHING=true` env parameter (#4175)

[1.11.10]
* Update FreeScout to 1.8.151
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.151)
* Added Kazakh language.
* Fixed "Undefined array key 1" error on fetching emails (#4195)
* Fixed checking POP3 connection when "PHP Request Shutdown: Unexpected characters at end of address" error occurs.
* Fixed non-multipart emails being fetched via POP3 with headers present in the body (#4181)
* Fixed out of memory error on Status page when there are many background jobs (#4197)

[1.11.11]
* Update FreeScout to 1.8.152
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.152)
* Fixed an issue with plain text emails having empty body - the issue has been introduced in the previous release (#4181)

[1.11.12]
* Update FreeScout to 1.8.153
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.153)
* Updated Polish translation.
* Updated German translation.
* Fixed an issue with emails being empty when fetching from iCloud (#4202)
* Changed /bin/sh to /bin/bash in update.sh (#4208)
* Improved @fwd detection when forwarding emails to FreeScout (#2940)
* Improved connection sleep time selection when fetching emails to avoid "connection setup failed" error (#4227)
* Avoid process list trancation when checking running commands on System Status page (#4237)
* Add CSRF check to the "Delete all files" link in Manage App Logs.

[1.11.13]
* Update FreeScout to 1.8.154
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.154)
* Fixed SMTP authentication not working in some cases due to setAuthMode() used in Swiftmailer - the issue has been introduced in v1.8.144 release (#4253)
* Fixed "null given" error in getMessageNumber() function (#4257)
* Check if current user is deleted on user's Permissions and Notifications pages.

[1.11.14]
* Update FreeScout to 1.8.155
* [Full changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.155)


[1.11.15]
* Update FreeScout to 1.8.156
* Fixed "Undefined variable $customer" error in thread.blade.php introduced in the previous release ([#&#8203;4321](https://github.com/freescout-helpdesk/freescout/issues/4321))
[1.11.16]
* Update freescout to 1.8.157
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.155)
* Updated Croatian translation.
* Fixed moving emails to a folder on fetching for mail servers not supporting `MOVE` command ([#&#8203;4313](https://github.com/freescout-helpdesk/freescout/issues/4313))
* Fixed `@fwd` forwarding when styles are present in the email body ([#&#8203;4333](https://github.com/freescout-helpdesk/freescout/issues/4333))
* Do not reset "Default Redirect" settings for users when saving Mailbox Permissions ([#&#8203;4339](https://github.com/freescout-helpdesk/freescout/issues/4339))
* Allow to set `APP_FETCHING_BUNCH_SIZE` env parameter ([#&#8203;4343](https://github.com/freescout-helpdesk/freescout/issues/4343))
* Fixed table wizard in the editor ([#&#8203;4345](https://github.com/freescout-helpdesk/freescout/issues/4345))

[1.11.17]
* Update freescout to 1.8.158
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.155)
* Fixed an issue with the `<span>` tag being added in the editor causing font size issues ([#&#8203;4354](https://github.com/freescout-helpdesk/freescout/issues/4354))
* Do not check `In-Reply-To` header when processing `@fwd` instruction ([#&#8203;4348](https://github.com/freescout-helpdesk/freescout/issues/4348))
* Removed `Webklex/laravel-imap` library and use `Webklex/php-imap` only.

[1.11.18]
* Update freescout to 1.8.159
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.155)
* Show more detailed errors on fetching ([#&#8203;4372](https://github.com/freescout-helpdesk/freescout/issues/4372))
* Optimized `UpdateFolderCounters` Job ([#&#8203;4374](https://github.com/freescout-helpdesk/freescout/issues/4374))
* Updated Spanish translation ([#&#8203;4376](https://github.com/freescout-helpdesk/freescout/issues/4376))
* Fixed text not being bolded - the issue has been introduced in the previous release ([#&#8203;4367](https://github.com/freescout-helpdesk/freescout/issues/4367))
* Fixed "Undefined array key 2" error on fetching ([#&#8203;4372](https://github.com/freescout-helpdesk/freescout/issues/4372))
* Fixed "Argument 1 (lastErrors) must be of type array" error in Carbon.
* Clear cache via `rm` in update.sh to avoid post-update issues ([#&#8203;4366](https://github.com/freescout-helpdesk/freescout/issues/4366))
* Added `Precedence:list` to the list of no auto-reply headers ([#&#8203;4363](https://github.com/freescout-helpdesk/freescout/issues/4363))

[1.11.19]
* Update freescout to 1.8.160
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.155)
* Updated Dutch translation.
* Added `aria-label` to the Notifications dropdown in the navigation menu ([#&#8203;4412](https://github.com/freescout-helpdesk/freescout/issues/4412))
* Fixed vars.js being broken when custom translations contain quotes ([#&#8203;4369](https://github.com/freescout-helpdesk/freescout/issues/4369))
* Added extra phrase to `no_retry_mail_errors` list ([#&#8203;870](https://github.com/freescout-helpdesk/freescout/issues/870))
* Improved `Helper::strSplitKeepWords()` function to work with Chinese symbols.
* Fixed OnOffSwitch checkboxes on screenreaders ([#&#8203;4412](https://github.com/freescout-helpdesk/freescout/issues/4412))
* Made "Insert Variable" dropdown accessible via screenreaders ([#&#8203;4412](https://github.com/freescout-helpdesk/freescout/issues/4412))
* Improved email parts boundary detection on fetching in Webklex/php-imap ([#&#8203;4417](https://github.com/freescout-helpdesk/freescout/issues/4417))
* Allow `class` attribute for tables in the purifier config ([#&#8203;4429](https://github.com/freescout-helpdesk/freescout/issues/4429))
* Fixed disappearing attachments when creating a new conversation from existing message ([#&#8203;4432](https://github.com/freescout-helpdesk/freescout/issues/4432))
* Make toolbar and dropdown buttons dynamic so they can be rearranged more easily ([#&#8203;4396](https://github.com/freescout-helpdesk/freescout/issues/4396))

[1.11.20]
* Update freescout to 1.8.161
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.155)
* Fixed "Call to a member function getFullName() on null" in Thread.php ([#&#8203;4438](https://github.com/freescout-helpdesk/freescout/issues/4438))
* Fixed "Implicit conversion from float to int" error in Guzzle ([#&#8203;4439](https://github.com/freescout-helpdesk/freescout/issues/4439))
* Fixed incorrect year returned by `User::dateFormat()` ([#&#8203;4443](https://github.com/freescout-helpdesk/freescout/issues/4443))
* Allow to get out of quote block in the editor ([#&#8203;3811](https://github.com/freescout-helpdesk/freescout/issues/3811))
* Fixed deleting attachments when creating a new conversation from existing thread ([#&#8203;4432](https://github.com/freescout-helpdesk/freescout/issues/4432))
* Do not remove customer phone when creating a phone conversation without specifying a customer phone number ([#&#8203;4464](https://github.com/freescout-helpdesk/freescout/issues/4464))

[1.11.21]
* Update freescout to 1.8.162
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.155)
* Updated Swedish language.
* [`Addeded`](https://github.com/freescout-helpdesk/freescout/commit/Addeded) filters allowing module customisation of email reply and user notification templates ([#&#8203;4477](https://github.com/freescout-helpdesk/freescout/issues/4477))
* Added `Asia/Colombo` timezone to timezone options ([#&#8203;4483](https://github.com/freescout-helpdesk/freescout/issues/4483))
* Fixed "Call to a member function getFullName() on null" when some user has been deleted from DB ([#&#8203;4467](https://github.com/freescout-helpdesk/freescout/issues/4467))
* Replace `fread()` with `fgets()` in Webklex/php-imap to avoid possible infinite loops.

[1.11.22]
* Update freescout to 1.8.163
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.163)
* Fixed user notification subject missing - issue introduced in the previous release ([#&#8203;4485](https://github.com/freescout-helpdesk/freescout/issues/4485))
* Fixed Waiting Since not showing -  issue introduced in the previous release ([#&#8203;4486](https://github.com/freescout-helpdesk/freescout/issues/4486))
* Webklex/php-imap: Fixed date issue if timezone is UT and a 2 digit year.
* Webklex/php-imap: Make the space optional after a comma separator.
* Webklex/php-imap: Query - infinite loop when start chunk > 1.
* Webklex/php-imap: Better connection check for IMAP.

[1.11.23]
* Update freescout to 1.8.164
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.164)
* Fixed an issue with opening conversations ("Accessing array offset on null") introduced in the previous release ([#&#8203;4488](https://github.com/freescout-helpdesk/freescout/issues/4488))

[1.11.24]
* Update freescout to 1.8.165
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.165)
* Fixed "Trying to access array offset on value of type null" error introduced in previous releases ([#&#8203;4490](https://github.com/freescout-helpdesk/freescout/issues/4490))

[1.11.25]
* Update freescout to 1.8.166
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.166)
* Webklex/php-imap: Boundary parsing fixed and improved to support more formats.
* Webklex/php-imap: "From" address parsing improved and extended to include more cases.
* Webklex/php-imap: Decode partially encoded address names.
* Webklex/php-imap: send NOOP connection check once in a second for IMAP.
* Webklex/php-imap: Fixed attachments with sspecial symbols in filename (; and =).
* Improved `MailHelper::decodeSubject()` function.
* Fixed "iconv_mime_decode(): Malformed string" error in `MailHelper::imapUtf8()`.
* Removed `<blockquote>` from the list of reply separators ([#&#8203;4460](https://github.com/freescout-helpdesk/freescout/issues/4460))
* Do not require `--mailbox` parameter in `freescout:parse-eml` command.

[1.11.26]
* Update freescout to 1.8.167
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.167)
* Fixed "Call to undefined function str_starts_with()" on fetching with PHP 7.x - introduced in the previous release ([#&#8203;4499](https://github.com/freescout-helpdesk/freescout/issues/4499))
* Improved PHP 8.4 compatibility ([#&#8203;4501](https://github.com/freescout-helpdesk/freescout/issues/4501))
* Exclude `Modules/` folder from composer classmap ([#&#8203;4496](https://github.com/freescout-helpdesk/freescout/issues/4496))

[1.11.27]
* Update freescout to 1.8.168
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.168)
* Fixed "Undefined variable $e" error in Header.php ([#&#8203;4505](https://github.com/freescout-helpdesk/freescout/issues/4505))
* Fixed accents and special characters removed from new customer names ([#&#8203;4506](https://github.com/freescout-helpdesk/freescout/issues/4506))
* Fixed empty message issue ([#&#8203;4508](https://github.com/freescout-helpdesk/freescout/issues/4508))
* Improved compatibility with PHP 8.4 ([#&#8203;4501](https://github.com/freescout-helpdesk/freescout/issues/4501))

[1.11.28]
* Update freescout to 1.8.169
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.169)
* Updated German translation.
* Make encoding check case-insensitive in Header.php ([#&#8203;4519](https://github.com/freescout-helpdesk/freescout/issues/4519))
* Increase mailbox `imap_sent_folder` field max length to 50 symbols ([#&#8203;4533](https://github.com/freescout-helpdesk/freescout/issues/4533))
* Fixed "getFullName() on null" error in Conversation.php ([#&#8203;4540](https://github.com/freescout-helpdesk/freescout/issues/4540))
* Fixed separating replies in Proton emails ([#&#8203;4537](https://github.com/freescout-helpdesk/freescout/issues/4537))
* Added extra Gmail reply separator.
* Fixed deleting attachments when creating a new conversation from existing thread ([#&#8203;4432](https://github.com/freescout-helpdesk/freescout/issues/4432))
* Remove services.php and packages.php from `/bootstrap/cache/` when clearing cache ([#&#8203;4536](https://github.com/freescout-helpdesk/freescout/issues/4536))
* Change PHP packages installation order to avoid installing Apache ([#&#8203;4514](https://github.com/freescout-helpdesk/freescout/issues/4514))
* Search by user fullname for conversations and customers ([#&#8203;4523](https://github.com/freescout-helpdesk/freescout/issues/4523))
* Revert editor blockquote fix ([#&#8203;3811](https://github.com/freescout-helpdesk/freescout/issues/3811))

[1.12.0]
* OIDC auth implemented

[1.12.1]
* add oidc to checklist

[1.13.0]
* Update freescout to 1.8.170
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.170)
* Fixed empty body issue introduced in the previous release ([#&#8203;4556](https://github.com/freescout-helpdesk/freescout/issues/4556))

[1.13.1]
* Update freescout to 1.8.171
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.171)
* Allow to set `PDO::PGSQL_ATTR_DISABLE_PREPARES` via .env variable ([#&#8203;4558](https://github.com/freescout-helpdesk/freescout/issues/4558))
* Allow to enable persistent connections for MySQL and PostgreSQL via .env variable ([#&#8203;4566](https://github.com/freescout-helpdesk/freescout/issues/4566))
* Updated Swedish translations ([#&#8203;4578](https://github.com/freescout-helpdesk/freescout/issues/4578))
* Fixed searching conversations and customers on PostgreSQL - issue introduced in the previous release ([#&#8203;4559](https://github.com/freescout-helpdesk/freescout/issues/4559))
* Fixed "Undefined variable $protonmail_quote_pos" on fetching - issue introduced in the previous release ([#&#8203;4568](https://github.com/freescout-helpdesk/freescout/issues/4568))
* Show which attachments are embedded in `freescout:parse-eml` command ([#&#8203;4575](https://github.com/freescout-helpdesk/freescout/issues/4575))

[1.13.2]
* Update freescout to 1.8.172
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.172)
* Merge customers ([#&#8203;2188](https://github.com/freescout-helpdesk/freescout/issues/2188))
* Handling boundary split into several parts in email headers ([#&#8203;4567](https://github.com/freescout-helpdesk/freescout/issues/4567))

[1.14.0]
* Update to correct base image 5.0.0

[1.15.0]
* Update freescout to 1.8.173
* [Full Changelog](https://github.com/freescout-helpdesk/freescout/releases/tag/1.8.173)
* Show customer ZIP in the customer sidebar ([#&#8203;4606](https://github.com/freescout-helpdesk/freescout/issues/4606))
* Fixed loadHTML() error on fetching ([#&#8203;4600](https://github.com/freescout-helpdesk/freescout/issues/4600))
* Allow to get out of code and citation blocks in the editor ([#&#8203;4607](https://github.com/freescout-helpdesk/freescout/issues/4607))
* Allow to specify the path to GitHub zip archive in module's `latestVersionZipUrl` parameter ([#&#8203;4611](https://github.com/freescout-helpdesk/freescout/issues/4611))
* Allow to specify module.json in module's `latestVersionNumberUrl` parameter ([#&#8203;4611](https://github.com/freescout-helpdesk/freescout/issues/4611))

