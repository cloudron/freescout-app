#!/bin/bash

set -eu

cd /app/code

echo "=> Run cron tasks"

# when run with --no-interaction it will not daemonize (unlike in the supervisor config for it)
/usr/local/bin/gosu www-data:www-data php artisan schedule:run --no-interaction
